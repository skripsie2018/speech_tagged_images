# Skripsie Project 2018

Student: T. Nel - 18179460@sun.ac.za

Supervising lecturer: Professor [H. Kamper](http://www.kamperh.com/)

Topic: KamperH2

Tagging (mobile phone) images with spoken descriptions

You will write an application which allows a user to "tag" images with spoken
descriptions, potentially on a mobile phone. This could be useful e.g. for blind
users. After tagging a few images with their speech, the user should be able to
search a collection of images using spoken queries. The idea would be to build
a rudimentary system tailored to a specific user; e.g. if the user tags all his/her
images in Afrikaans, the system should still be able to retrieve relevant images
when presented with Afrikaans queries. There is scope to focus either more on
application development or on matching methods.

# A quick note on Anaconda-Project and Virtual Environments

To prevent incompatibilities and make this project portable, [Anaconda Project](https://anaconda-project.readthedocs.io/en/latest/) was used.

It is an extension of the Anaconda Python distribution that features its own virtual environment and package manager, among other features.

Note that it therefore installs all its own Python packages within its virtual environment, and runs them there as well.

You do not need to manually install dependencies not available on Anaconda's repository (just follow the steps in [Get Started](#Get Started), but they are listed below in any event:

## Dependencies

- [python_speech_features](https://github.com/jameslyons/python_speech_features)
- [speech_dtw](https://github.com/gorrox/speech_dtw) (a fork of Professor Kamper's code)

## Data requirements

For this project, audio-captions filenames' are required to contain the filename of the image that they are a caption to.
Images and audio should be in separate directories.

Images must be in .jpg format. Audio must be in .wav format.

### Example:

- images/img05.jpg
- images/img06.jpg
- ...

- audio/aud02_img05.wav
- audio/aud03_img06.wav
- ...

# Get Started

This project was done on a Linux machine, Windows compatibility is pending.

Install [Anaconda](https://www.anaconda.com/download/#linux) 

Version used for this project: [Anaconda2-5.2.0-Linux-x86_64.sh](https://repo.continuum.io/archive/Anaconda2-5.2.0-Linux-x86_64.sh) (Python 2.7 version)

Open a terminal in the project root folder, then run the following commands in sequence:

- ```anaconda-project prepare```
- ```anaconda-project run i-local-deps```
- ```anaconda-project run i-tensorflow```
- ```anaconda-project run dl-data```

This process may take a while. When it is finished you can run:

```anaconda-project run notebook```

## The Anaconda Project-commands

You can now list the possible project commands using: ```anaconda-project list-commands```

You can run them using:

```anaconda-project run [command]```

### List of run commands

All ```run``` commands do their task within the Anaconda Project virtual environment

- ```(default)``` : ```anaconda-project run``` or ```anaconda-project run default``` will run main.py
- ```i-local-deps``` : Installs required local packages
- ```i-tensorflow``` : Installs the required tensorflow package (could not be included via default pip)
- ```dl-data``` : Downloads required data (pending Anaconda-Project fix)
- ```notebook``` : Opens the Project Jupyter Notebook
- ```eg_dtw_qbe``` : Opens Prof. H. Kamper's DTW Query by Example Jupyter Notebook (test that his package works)

### Other commands

- ```anaconda-project clean``` : Cleans the envs (environment) file and other project files

# Creating A Redistributable Package

After finishing [Get Started](#Get Started), you can create the redistributable package as follows:

```anaconda-project archive speech_tagged_images.tar.gz```