from datetime import datetime
import time
from os import path
from scipy.misc import imread, imresize
import glob
import numpy as np
import os
import tensorflow as tf
from speech_dtw import qbe
import vgg16
from python_speech_features import mfcc
from python_speech_features import delta
import scipy.io.wavfile as wav
from multiprocessing import cpu_count
import operator
import random
from scipy.spatial.distance import cosine

TF_DTYPE = tf.float32
TF_ITYPE = tf.int32

FILE_DIR = os.path.dirname(__file__)
DATA_DIR = os.path.join(FILE_DIR, os.pardir, 'data')
CACHED_PATHS = os.path.join(DATA_DIR, "paths.txt")
VGG16_WEIGHTS_DIR = os.path.join(DATA_DIR, "vgg16_weights.npz")
VECTORISED_IMAGES_DIR = os.path.join(DATA_DIR, "images_fc7.npz")
VECTORISED_AUDIO_MFCC_DIR = os.path.join(DATA_DIR, "audio_mfcc.npz")
VECTORISED_AUDIO_FBANK_DIR = os.path.join(DATA_DIR, "audio_fbank.npz")
IMAGE_LABELS_DIR = os.path.join(DATA_DIR, "image_labels.npz")
DTW_TEST_DICT_DIR = os.path.join(DATA_DIR, "dtw_test_dict.npz")
CNN_TEST_DICT_DIR = os.path.join(DATA_DIR, "cnn_test_dict.npz")
BB_TEST_DICT_DIR = os.path.join(DATA_DIR, "dd_test_dict.npz")

def boolInput(req):
    """Requests a yes/no input from user and returns boolean answer"""
    while(True):
        val = raw_input(req)
        if val.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif val.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            print("Boolean value expected")

def pathInput(req):
    """Requests a directory to some data and returns it if valid"""
    while(True):
        dir = raw_input(req)
        if(os.path.isdir(dir)):
            return dir
        else:
            print("Not a valid directory")

def getCachedPaths():
    """Returns paths cached for the images and audio"""
    f = open(CACHED_PATHS, "r")
    lines = f.read().splitlines()
    images_dir = lines[0]
    print("Cached images directory: " + images_dir)
    audio_dir = lines[1]
    print("Cached audio directory: " + audio_dir)
    f.close()
    return images_dir, audio_dir

def writePathsCache(images_dir, audio_dir):
    """Overwrite paths.txt with new directories"""
    f = open(CACHED_PATHS, "w+")
    f.write(images_dir + "\n")
    f.write(audio_dir)
    f.close()

def printSampleFilenames(images, audio, num_rand_entries):
    """Print 5 sample filenames for the image and audio datasets respectively"""
    print("Sample image filenames:")
    image_name_samples = random.sample(images.keys(), num_rand_entries)
    for name in image_name_samples:
        print(name)
    print("\nSample audio filenames:")
    audio_name_samples = random.sample(audio.keys(), num_rand_entries)
    for name in audio_name_samples:
        print(name)


def getQueryName(dataset):
    """Get the filename of the audio query and return it if it is valid"""
    while(True):
        fname = raw_input("Please enter the audio filename you want to use as the query:")
        if fname in dataset.keys():
            return fname
        else:
            print("Invalid file name")

def setupDirs():
    """Returns external raw-data directories""" 
    flag = True
    if os.path.isfile(CACHED_PATHS):
        flag = boolInput("Would you like to change images & audio paths? (y/n)")
    if flag:
        images_dir = pathInput("Enter the directory containing jpg images:")
        audio_dir = pathInput("Enter the directory containing audio captions:")
        writePathsCache(images_dir, audio_dir)
    else:
        images_dir, audio_dir = getCachedPaths()
    return images_dir, audio_dir

def setupVecData(images_dir, audio_dir, skip_prompt=False):
    """Returns the vectorised images and audio - prompts to create new files if they don't exist"""
    # Images
    #TODO: Cross-reference and vectorise new images, recompile npz
    flag = True
    if os.path.isfile(VECTORISED_IMAGES_DIR) and not skip_prompt:
        flag = boolInput("Would you like to revectorise the images? This may take a considerable length of time (y/n)")
    if flag:
        try:
            batch_size = int(raw_input("Enter batch size value (default max 50):"))
        except ValueError:
            print("Not a whole number, reverting to max batch size of 50")
            batch_size = 50
        vectorise_images(images_dir, VGG16_WEIGHTS_DIR, batch_size, "fc7", VECTORISED_IMAGES_DIR) #Using fc7 by default
    vectorised_images = np.load(VECTORISED_IMAGES_DIR)

    # Audio
    flag = True
    if os.path.isfile(VECTORISED_AUDIO_MFCC_DIR) and not skip_prompt:
        flag = boolInput("Would you like to revectorise the audio? This may take a considerable length of time (y/n)")
    if flag:
        vectorise_audio(audio_dir, VECTORISED_AUDIO_MFCC_DIR)
    vectorised_audio_mfcc = np.load(VECTORISED_AUDIO_MFCC_DIR)
    #vectorised_audio_fbank = np.load(VECTORISED_AUDIO_FBANK_DIR)

    return vectorised_images, vectorised_audio_mfcc

def images_from_dir(directory, extension="jpg"):
    """Returns dictionary of image file names and data"""
    images_dict = {}
    for image_fn in sorted(glob.glob(path.join(directory, "*." + extension))):
        image = imread(image_fn, mode="RGB")
        image = imresize(image, (224, 224))
        images_dict[path.splitext(path.split(image_fn)[-1])[0]] = image
    return images_dict

def audio_from_dir(directory, extension="wav"):
    """Returns dictionary of audio file names and data"""
    audio_dict = {}
    for audio_fn in sorted(glob.glob(path.join(directory, "*." + extension))):
        (rate, signal) = wav.read(audio_fn)
        audio = {"rate":rate, "signal":signal}
        audio_dict[path.splitext(path.split(audio_fn)[-1])[0]] = audio
    return audio_dict

# Vectorise images using VGG16
def vectorise_images(images_dir, weights_path, batch_size, output_layer, output_path):
    print("Vectorising images in " + images_dir + " with a batch size of " + str(batch_size))
    
    # Slurm options
    if "OMP_NUM_THREADS" in os.environ:
        num_threads = int(os.environ["OMP_NUM_THREADS"])
        config = tf.ConfigProto(intra_op_parallelism_threads=num_threads) #, log_device_placement=True)
    else:
        config = None

    # Data
    print datetime.now()
    print "Reading directory:", images_dir
    images_dict = images_from_dir(images_dir)
    image_keys = sorted(images_dict.keys())
    print datetime.now()
    print "No. images:", len(images_dict)
    # Limit batch size if necessary
    if len(images_dict) < batch_size:
        batch_size = len(images_dict)
        print("Limiting batch size to" + str(batch_size))
    input_x = np.array([images_dict[i] for i in image_keys])

    # Batch feed iterator
    print "Batch size:", batch_size
    class BatchFeedIterator(object):
        def __init__(self, x_mat):
            self._x_mat = x_mat
        def __iter__(self):
            n_batches = int(np.float(self._x_mat.shape[0] / batch_size))
            for i_batch in xrange(n_batches):
                yield (
                    self._x_mat[
                        i_batch * batch_size:(i_batch + 1) * batch_size
                        ]
                    )
    apply_batch_iterator = BatchFeedIterator(input_x)

    # Pretrained weights
    print(weights_path)
    assert path.isfile(weights_path)
    print "Reading:", weights_path
    weights = np.load(weights_path)

    # Build model
    x = tf.placeholder(TF_DTYPE, [None, 224, 224, 3])
    vgg_dict, parameters = vgg16.build_vgg16(x)
    print "VGG output:", output_layer
    vgg_output = vgg_dict[output_layer]

    # Run model
    print datetime.now()
    init = tf.initialize_all_variables()
    with tf.Session(config=config) as session:
        session.run(init)

        print "Pretrained parameters"
        for i, key in enumerate(sorted(weights.keys())):
            print("{}: {}".format(key, list(weights[key].shape)))
            session.run(parameters[i].assign(weights[key]))

        # Apply model to batches
        output_dict = {}
        n_outputs = 0
        print datetime.now()
        print "Passing batches through model"
        for cur_feed in apply_batch_iterator:
            cur_output = session.run(vgg_output, feed_dict={x: cur_feed})
            for i in xrange(cur_output.shape[0]):
                # print "-"*79
                # print image_keys[0]
                output_dict[image_keys.pop(0)] = cur_output[i, :]
                n_outputs += 1
                # from imagenet_classes import class_names
                # predictions = (np.argsort(cur_output[i, :])[::-1])[0:5]
                # for p in predictions:
                #     print class_names[p], cur_output[i, :][p]

        print "Processed {} inputs out of {}".format(n_outputs, input_x.shape[0])

    print datetime.now()
    print "Writing:", output_path
    np.savez_compressed(output_path, **output_dict)

# Vectorise single audio file using MFCCs : https://github.com/gorrox/speech_dtw (original code by Prof H Kamper)
def get_mfcc_dd(wav_fn, cmvn=True):
    """Return the MFCCs with deltas and delta-deltas for a audio file."""
    rate = wav_fn["rate"]
    signal = wav_fn["signal"]
    mfcc_static = mfcc(signal, rate)
    mfcc_deltas = delta(mfcc_static, 2)
    mfcc_delta_deltas = delta(mfcc_deltas, 2)
    features = np.hstack([mfcc_static, mfcc_deltas, mfcc_delta_deltas])
    if cmvn:
        features = (features - np.mean(features, axis=0)) / np.std(features, axis=0)
    return features

# Vectorise audio using MFCCs
def vectorise_audio(audio_dir, output_path):
    # Data
    print datetime.now()
    print "Reading directory:", audio_dir
    audio_dict = audio_from_dir(audio_dir)
    audio_keys = sorted(audio_dict.keys())
    print datetime.now()
    print "No. audios:", len(audio_dict)
    
    # Extract features
    audio_mfcc_dict = {}
    for key in audio_keys:
        audio_entry = audio_dict[key]
        audio_mfcc = get_mfcc_dd(audio_entry)
        audio_mfcc_dict[key] = audio_mfcc
    
    # Save data
    print datetime.now()
    print "Writing:", output_path
    np.savez_compressed(output_path, **audio_mfcc_dict)

# Compare query audio to image caption(s) using DTW
def audio_query(query, dataset):
    """Query the dataset using the query file, with a given threshold (default=0.38)
    Returns a ranked list of tuples with the corresponding dtw cost for each audio file"""
    num_CPUs = cpu_count()
    #print("Detected " + str(num_CPUs) + " available CPUs")
    query_features = np.asarray(query, dtype=np.float64)
    dataset_features = [] #convert to list for function
    for key in dataset:
        dataset_features.append(np.asarray(dataset[key], dtype=np.float64))
    dtw_costs = qbe.parallel_dtw_sweep_min([query_features], dataset_features, n_cpus=num_CPUs)
    costs_dict = dict(zip(dataset.keys(), dtw_costs[0]))
    ranked_list = sorted(costs_dict.items(), key=operator.itemgetter(1))
    #del ranked_list[0] #should not include query
    return ranked_list

def extract_image_names_from_audio_names(image_names, dtw_costs, num_costs):
    """Returns list of the image filename substrings from the first num entries of audio filenames in dtw_costs"""
    audio_names = []
    for i in range(num_costs):
        audio_names.append(dtw_costs[i][0])
    
    names_list = []
    for img in image_names:
        if any(img in aud for aud in audio_names):
            if (img not in names_list): # prevent duplicates
                names_list.append(img) # this is unordered!

    ordered_names_list = []
    for aud in audio_names:
        for name in names_list:
            if name in aud:
                ordered_names_list.append(name)
                break

    return ordered_names_list

def get_untagged_image_names(audio_names, image_names):
    """Returns the filenames of the images which do not have audio captions"""
    untagged_images = list(image_names)
    for img in image_names:
        if any(img in aud for aud in audio_names):
            untagged_images.remove(img)
    return untagged_images

# Compare untagged image vectors to best match image(s)
def rank_untagged_images(vectorised_images, untagged_images_names, dtw_costs):
    """Returns a ranked list of image filenames that have been compared to the best ranked tagged-images"""
    score_dict = {}

    #TODO: Advanced aggregation. For now, compare only to best-match image
    ref_img_name = extract_image_names_from_audio_names(vectorised_images.keys(), dtw_costs, 1)[0]
    ref_img = vectorised_images[ref_img_name]

    score_list = []
    for img_name in untagged_images_names:
        tagged_img = vectorised_images[img_name]
        score_list.append(np.linalg.norm(ref_img-tagged_img)) #L2 distance
                
    minm = min(score_list)
    maxm = max(score_list)
    scaled_score_list = [((x-minm)/(maxm-minm)) for x in score_list]
    score_dict = dict(zip(untagged_images_names, scaled_score_list))
    return sorted(score_dict.items(), key=operator.itemgetter(1))

def rank_untagged_images_cosine(vectorised_images, untagged_images_names, dtw_costs):
    """Returns a ranked list of image filenames that have been compared to the best ranked tagged-images"""
    score_dict = {}

    #TODO: Advanced aggregation. For now, compare only to best-match image
    ref_img_name = extract_image_names_from_audio_names(vectorised_images.keys(), dtw_costs, 1)[0]
    ref_img = vectorised_images[ref_img_name]

    score_list = []
    for img_name in untagged_images_names:
        tagged_img = vectorised_images[img_name]
        score_list.append(cosine(ref_img,tagged_img)) #Cosine distance
        
    score_dict = dict(zip(untagged_images_names, score_list))
    return sorted(score_dict.items(), key=operator.itemgetter(1))

def rank_images_vector_avg_Euclidean(vectorised_images, ref_image_names, target_image_names, N=10):
    """Returns a ranked list of target images compared to average over the first N reference images using Euclidean distance"""
    score_dict = {}

    ref_images = [vectorised_images[image_name] for image_name in ref_image_names] #list of lists
    ref_vector_avg = np.mean(np.array(ref_images[:N]), axis=0)

    score_list = []
    for img_name in target_image_names:
        target_img = vectorised_images[img_name]
        score_list.append(np.linalg.norm(ref_vector_avg-target_img)) #Cosine distance
        
    minm = min(score_list)
    maxm = max(score_list)
    scaled_score_list = [((x-minm)/(maxm-minm)) for x in score_list]
    score_dict = dict(zip(target_image_names, scaled_score_list))
    return sorted(score_dict.items(), key=operator.itemgetter(1))

def rank_images_vector_avg_cosine(vectorised_images, ref_image_names, target_image_names, N=10):
    """Returns a ranked list of target images compared to average over the first N reference images using Cosine distance"""
    score_dict = {}

    ref_images = [vectorised_images[image_name] for image_name in ref_image_names] #list of lists
    ref_vector_avg = np.mean(np.array(ref_images[:N]), axis=0)

    score_list = []
    for img_name in target_image_names:
        target_img = vectorised_images[img_name]
        score_list.append(cosine(ref_vector_avg,target_img)) #Cosine distance
        
    score_dict = dict(zip(target_image_names, score_list))
    return sorted(score_dict.items(), key=operator.itemgetter(1))

# EVALUATION METRICS

def get_dtw_eval_metrics(target, ranked_results, threshold, rev_thresh=False):
    """Return the TPR, FPR, precision and recall for a trial at some threshold"""
    if not rev_thresh:
        selected = [entry[0] for entry in ranked_results if entry[1] > threshold]
    else:
        selected = [entry[0] for entry in ranked_results if entry[1] < threshold]
    
    TP = [entry for entry in selected if target in entry] # True Positives
    P = [entry[0] for entry in ranked_results if target in entry[0]] # all Positives

    TPR = float(len(TP))/float(len(P)) if len(P) != 0 else 0.0 # True Positive Rate
    FPR = float((len(selected)-len(TP)))/float((len(ranked_results)-len(P))) # False Positive Rate

    #precision = 0
    #recall = 0
    precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
    recall = TPR # how many correct results did I miss?

    return TPR, FPR, precision, recall

def get_cnn_eval_metrics(targets, labels_dict, ranked_results, threshold, rev_thresh=False):
    """Return the TPR, FPR, precision and recall for a trial at some threshold"""
    if not rev_thresh:
        selected = [entry[0] for entry in ranked_results if entry[1] > threshold]
    else:
        selected = [entry[0] for entry in ranked_results if entry[1] < threshold]

    #print("Ranked results:" + str(len(ranked_results)))
    #print("Selected: " + str(len(selected)))

    TP = [entry for entry in selected if any([target for target in targets if target in labels_dict[entry]])] # True Positives
    P = [entry[0] for entry in ranked_results if any([target for target in targets if target in labels_dict[entry[0]]])] # all Positives

    #print("TP: " + str(len(TP)))
    #print("P: " + str(len(P)))
    
    TPR = float(len(TP))/float(len(P)) # True Positive Rate
    FPR = float((len(selected)-len(TP)))/float((len(ranked_results)-len(P))) # False Positive Rate

    #print("FPR: " + str(FPR))
    #print("TPR: " + str(TPR))


    return TPR, FPR

def dtw_ROC(vectorised_audio, resolution=100, num_random_trials=10):
    """Return the resolution-length ROC sequence for the DTW algorithm, constructed from a specified number of random query trials"""

    # Choose random queries
    random_queries = random.sample(vectorised_audio.keys(), num_random_trials)

    TPR_axes = []
    FPR_axes = []
    # For each query, ROC
    for query in random_queries:
        dtw_costs = audio_query(vectorised_audio[query], vectorised_audio)
        target = query.split('_')[0]
        # Form ROC by getting TPR and FPR for each threshold
        thresholds = np.arange(0,1,(1.0/resolution))
        TPR_axis = []
        FPR_axis = []
        for threshold in thresholds:
            TPR, FPR, precision, recall = get_dtw_eval_metrics(target, dtw_costs, threshold, rev_thresh=True)
            TPR_axis.append(TPR)
            FPR_axis.append(FPR)
            #print("Precision: " + precision + "\tRecall: " + recall)
        TPR_axes.append(TPR_axis)
        FPR_axes.append(FPR_axis)

    # Average over the trials
    ROC_TPR_axis = np.mean(np.array(TPR_axes), axis=0)
    ROC_FPR_axis = np.mean(np.array(FPR_axes), axis=0)

    return ROC_FPR_axis, ROC_TPR_axis

def cnn_ROC(vectorised_images, sample_size=1000, resolution = 100, num_random_trials=10):
    """Return the resolution-length ROC sequence for the CNN algorithm, constructed from a specified number of random query trials"""
    # Get tagged image names
    image_names = vectorised_images.keys()
    print("Total number of images: " + str(len(image_names)))

    # Get dictionary of labels
    labels_dict = np.load(IMAGE_LABELS_DIR)

    # Remove mismatches
    #TODO: Temporary workaround
    #for img_name in image_names:
    #    if img_name not in labels_dict.keys():
    #        image_names.remove(img_name)
    trimmed_image_names = [img_name for img_name in image_names if img_name in labels_dict.keys()]
    print("Total number of labelled images: " + str(len(trimmed_image_names)))
                             
    # Choose random queries                         
    random_queries = random.sample(trimmed_image_names, num_random_trials)
                             
    TPR_axes = []
    FPR_axes = []
    
    # For each query, ROC
    for query in random_queries:
        start_time = time.time()
        # Rank the images
        ref_img = vectorised_images[query]
        score_list = []
        image_names_cpy = list(trimmed_image_names)
        image_names_cpy.remove(query) # Query will have zero-distance score, throws off results

        image_names_subset = random.sample(image_names_cpy, sample_size) # Grab random images for comparison to speed up analysis
        #image_names_subset = list(image_names_cpy) #Or, use all of them

        for img_name in image_names_subset:
            tagged_img = vectorised_images[img_name]
            #score_list.append(np.linalg.norm(ref_img-tagged_img)) # L2 distance
            score_list.append(cosine(ref_img,tagged_img)) #Cosine distance
            
        minm = min(score_list)
        maxm = max(score_list)
        scaled_score_list = [((x-minm)/(maxm-minm)) for x in score_list]
        score_dict = dict(zip(image_names_subset, scaled_score_list))
        #ranked_images = sorted(score_dict.items(), key=operator.itemgetter(1)) # for L2
        ranked_images = sorted(score_dict.items(), key=operator.itemgetter(1))
        
        # Get the targets
        #TODO: Advanced aggregation - Currently just using best result
        targets = labels_dict[ranked_images[0][0]]
        print(targets)
        
        # Form ROC by getting TPR and FPR for each threshold
        thresholds = np.arange(0,1,(1.0/resolution))
        TPR_axis = []
        FPR_axis = [] 
        i=0.0
        for threshold in thresholds:
            print("Thresh: " + str(i))
            i=i+(1.0/resolution)
            TPR, FPR = get_cnn_eval_metrics(targets, labels_dict, ranked_images, threshold, rev_thresh=True)
            TPR_axis.append(TPR)
            FPR_axis.append(FPR)
        TPR_axes.append(TPR_axis)
        FPR_axes.append(FPR_axis)

        elapsed_time = time.time() - start_time
        time_str = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        print("Trial complete " + time_str)
    
    # Average over the trials
    ROC_TPR_axis = np.mean(np.array(TPR_axes), axis=0)
    ROC_FPR_axis = np.mean(np.array(FPR_axes), axis=0)
    
    return ROC_FPR_axis, ROC_TPR_axis
    
def dtw_p_at_10(vectorised_audio):
    """Returns the value of the precision at 10, converged over a number of trials"""

    p_at_10 = 0.0
    old_p_at_10 = 0.0 #use for convg comparison
    b_convg = False
    used_targets = [] #don't repeat a trial
    trial_results = []

    while(not b_convg):
        start_time = time.time()
        target = ""
        flag = True
        while(flag):
            query = random.sample(vectorised_audio.keys(), 1)[0]
            target = query.split('_')[0]
            if target not in used_targets:
                flag = False
                used_targets.append(target)
    
        dtw_costs = audio_query(vectorised_audio[query], vectorised_audio)
        selected = [entry[0] for entry in dtw_costs[:10]]
        TP = [entry for entry in selected if target in entry] # True Positives
        precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
        trial_results.append(precision)
        p_at_10 = np.sum(trial_results)/float(len(trial_results)) #Average over trial results
        p_at_10 = round(p_at_10, 2) # Round to 2 decimal places
        elapsed_time = time.time() - start_time
        time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        print('Trial ' + str(len(trial_results)) + ' : ' + time_display + ' : ' + str(p_at_10))
        if p_at_10 == old_p_at_10: # Check for convergence
            b_convg = True
        else:
            old_p_at_10 = p_at_10
    
    return p_at_10

def dtw_p_at_10_n(vectorised_audio, num_trials):
    """Returns the value of the precision at 10, averaged over a number of trials"""

    p_at_10 = 0.0
    old_p_at_10 = 0.0 #use for convg comparison
    used_targets = [] #don't repeat a trial
    trial_results = []
    start_time = time.time()

    for i in range(num_trials):
        
        target = ""
        flag = True
        while(flag):
            query = random.sample(vectorised_audio.keys(), 1)[0]
            target = query.split('_')[0]
            if target not in used_targets:
                flag = False
                used_targets.append(target)

        dtw_costs = audio_query(vectorised_audio[query], vectorised_audio)
        selected = [entry[0] for entry in dtw_costs[:10]]
        TP = [entry for entry in selected if target in entry] # True Positives
        precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
        trial_results.append(precision)

    p_at_10 = np.sum(trial_results)/float(len(trial_results)) #Average over trial results
    p_at_10 = round(p_at_10, 2) # Round to 2 decimal places
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('Trials: ' + str(len(trial_results)) + ' : ' + time_display + ' : ' + str(p_at_10))
    
    return p_at_10

def dtw_p_at_N_n(vectorised_audio, num_trials):
    """Returns the value of the precision at N, averaged over a number of trials"""
    p_at_N = 0.0
    used_targets = [] #don't repeat a trial
    trial_results = []
    start_time = time.time()

    for i in range(num_trials):
        
        target = ""
        flag = True
        while(flag):
            query = random.sample(vectorised_audio.keys(), 1)[0]
            target = query.split('_')[0]
            if target not in used_targets:
                flag = False
                used_targets.append(target)

        dtw_costs = audio_query(vectorised_audio[query], vectorised_audio)
        P = [entry[0] for entry in dtw_costs if target in entry[0]] # all Positives
        N = len(P)
        selected = [entry[0] for entry in dtw_costs[:N]] # select first N
        TP = [entry for entry in selected if target in entry] # True Positives
        precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
        trial_results.append(precision)

    p_at_N = np.sum(trial_results)/float(len(trial_results)) #Average over trial results
    p_at_N = round(p_at_N, 2) # Round to 2 decimal places
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('Trials: ' + str(len(trial_results)) + ' : ' + time_display + ' : ' + str(p_at_N))
    
    return p_at_N

def get_EER_index(ROC_FPR_axis, ROC_TPR_axis):
    smallest = abs(ROC_FPR_axis[0] - ROC_TPR_axis[0])
    index = 0
    for i in range(1, len(ROC_FPR_axis)):
        dist = abs(ROC_FPR_axis[i] - ROC_TPR_axis[i])
        if dist < smallest:
            smallest = dist
            index = i

    return index

def make_dtw_test_dict(vectorised_audio, num_queries, num_tags):
    """Choose a number of sample audio files and save a 2D dict of dtw cost values, returns the dict"""
    start_time = time.time()
    print('Computing new DTW testing dict')

    # Only reference images that have labels in the dictionary
    labels_dict = np.load(IMAGE_LABELS_DIR)
    vectorised_images = np.load(VECTORISED_IMAGES_DIR)
    labelled_image_names = [image_name for image_name in vectorised_images if image_name in labels_dict]
    labelled_audio_names = [audio_name for audio_name in vectorised_audio if any(image_name for image_name in labelled_image_names if image_name in audio_name)]

    # Choose combined subset
    audio_names_subset = random.sample(labelled_audio_names, (num_queries + num_tags))

    # Choose queries
    queries_names = random.sample(audio_names_subset, num_queries)
    queries = {}
    for name in queries_names:
        queries[name] = vectorised_audio[name]

    # Choose tags
    tags_names = [name for name in audio_names_subset if name not in queries_names]
    tags = {}
    for name in tags_names:
        tags[name] = vectorised_audio[name]

    # Compute DTW costs for each query
    dtw_dict = {}
    for query in queries:
        dtw_costs = audio_query(queries[query], tags)
        dtw_dict[query] = dtw_costs
    
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('Compiled dtw_dict in: ' + time_display)

    return dtw_dict

def get_dtw_test_dict(vectorised_audio, num_queries, num_tags, override=False):
    """Get a dict containing a ranked list of tuples for each audio file. Creates a new file if one does not exist"""
    dtw_dict = {}
    if os.path.isfile(DTW_TEST_DICT_DIR) and not override:
        dtw_dict_file = np.load(DTW_TEST_DICT_DIR)
        # convert strings to floats
        for query in dtw_dict_file:
            ranked_tuples = dtw_dict_file[query]
            dtw_dict[query] = []
            for ranked_tuple in ranked_tuples:
                new_tuple = (ranked_tuple[0], float(ranked_tuple[1])) #conversion
                dtw_dict[query].append(new_tuple)
        if len(dtw_dict) != num_queries or len(dtw_dict[dtw_dict.keys()[0]]) != num_tags:
            dtw_dict = make_dtw_test_dict(vectorised_audio, num_queries, num_tags)
            np.savez_compressed(DTW_TEST_DICT_DIR, **dtw_dict)
    else:
        dtw_dict = make_dtw_test_dict(vectorised_audio, num_queries, num_tags)
        np.savez_compressed(DTW_TEST_DICT_DIR, **dtw_dict)
    
    return dtw_dict

def get_dtw_FPR_TPR(target, ranked_tuples, threshold, rev_thresh=False):
    """Return the FPR & TPR for a DTW ranked list of tuples at some threshold"""
    # ranked_tuple[0] is name
    # ranked_tuple[1] is score

    if not rev_thresh:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] > threshold]
    else:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] < threshold]
    
    TP = [name for name in selected if target in name] # True Positives
    P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if target in ranked_tuple[0]] # all Positives

    TPR = float(len(TP))/float(len(P)) if len(P) != 0 else 0.0 # True Positive Rate
    FPR = float((len(selected)-len(TP)))/float((len(ranked_tuples)-len(P))) # False Positive Rate

    return FPR, TPR

def get_dtw_metrics(dtw_dict, ROC_res=100):
    """Calculate P@10, P@N, ROC curve and EER for DTW audio-similarity comparison"""
    p_at_10 = 0.0
    p_at_N = 0.0
    FPR_axis = []
    TPR_axis = []
    EER_index = 0

    # P@10
    start_time = time.time()
    p_at_10_results = []
    for query in dtw_dict:
        ranked_tuples = dtw_dict[query]
        target = query.split('_')[0]
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:10]]
        TP = [name for name in selected if target in name] # True Positives
        precision = float(len(TP))/float(len(selected)) #if len(selected) != 0 else 0 # how many of the returned results correct?

        # DEBUG
        #print(query)
        #print(target)
        #print(selected)
        #print(TP)
        #print(precision)
        #print(' ')

        p_at_10_results.append(precision)
    p_at_10 = np.mean(p_at_10_results) #Average over trial results
    p_at_10 = round(p_at_10, 2) # Round to 2 decimal places
    std_dev_10 = np.std(p_at_10_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@10: ' + str(p_at_10) + ' with std dev of: ' + str(std_dev_10) + ' - completed in ' + time_display)

    # P@N
    start_time = time.time()
    p_at_N_results = []
    for query in dtw_dict:
        ranked_tuples = dtw_dict[query]
        target = query.split('_')[0]
        P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if target in ranked_tuple[0]] # all Positives
        N = len(P)
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:N]] # select first N
        TP = [name for name in selected if target in name] # True Positives
        precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
        p_at_N_results.append(precision)
    p_at_N = np.mean(p_at_N_results) #Average over trial results
    p_at_N = round(p_at_N, 2) # Round to 2 decimal places
    std_dev_N = np.std(p_at_N_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@N: ' + str(p_at_N) + ' with std dev of: ' + str(std_dev_N) + ' - completed in ' + time_display)

    # ROC Curve
    start_time = time.time()
    TPR_axes = []
    FPR_axes = []
    # For each query, ROC
    for query in dtw_dict:
        ranked_tuples = dtw_dict[query]
        target = query.split('_')[0]
        # Form ROC by getting TPR and FPR for each threshold
        thresholds = np.arange(0,1,(1.0/ROC_res))
        TPR_axis = []
        FPR_axis = []
        for threshold in thresholds:
            FPR, TPR = get_dtw_FPR_TPR(target, ranked_tuples, threshold, rev_thresh=True)
            TPR_axis.append(TPR)
            FPR_axis.append(FPR)
        TPR_axes.append(TPR_axis)
        FPR_axes.append(FPR_axis)
    # Average over the trials
    TPR_axis = np.mean(np.array(TPR_axes), axis=0)
    FPR_axis = np.mean(np.array(FPR_axes), axis=0)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('ROC computed in: ' + time_display)

    # EER
    eer_line = []
    for x in FPR_axis:
        eer_line.append(-x + 1)
    EER_index = np.argwhere(np.diff(np.sign(TPR_axis - eer_line))).flatten()
    print('EER threshold: ' + str(EER_index*(1.0/ROC_res)))

    return p_at_10, p_at_N, FPR_axis, TPR_axis, EER_index

def make_cnn_test_dict(vectorised_images, vectorised_audio, dtw_dict, num_untagged_images):
    """Choose a number of sample image files and save a 2D dict of cosine similarity values, relative to dtw_dict results, returns the dict"""
    start_time = time.time()
    print('Computing new CNN testing dict')

    # Get dictionary of labels
    labels_dict = np.load(IMAGE_LABELS_DIR)

    # Get names and images of queries from DTW dict
    query_image_dict = {}
    query_image_names = []
    for query in dtw_dict:
        ranked_tuples = dtw_dict[query]
        for ranked_tuple in ranked_tuples:
            audio_name = ranked_tuple[0]
            segments = audio_name.split('_')
            image_name = '_'.join([segments[2],segments[3]])
            if image_name not in query_image_names:
                query_image_names.append(image_name)
    for image_name in query_image_names:
        if image_name not in vectorised_images.keys():
            print(image_name + " not found in archive (query)")
        else:
            query_image_dict[image_name] = vectorised_images[image_name]
    
    # Pick random names & images of untagged images
    untagged_image_names = get_untagged_image_names(vectorised_audio.keys(), vectorised_images.keys())
    labelled_untagged_image_names = [image_name for image_name in untagged_image_names if image_name in labels_dict]
    untagged_image_names_subset = random.sample(labelled_untagged_image_names, num_untagged_images)
    untagged_image_dict = {}
    for image_name in untagged_image_names_subset:
        if image_name not in vectorised_images.keys():
            print(image_name + " not found in archive (sample)")
        else:
            untagged_image_dict[image_name] = vectorised_images[image_name]

    # Cosine similarity rankings
    cnn_dict = {}
    for query_image_name in query_image_dict:
        ref_image = query_image_dict[query_image_name]
        score_list = []
        for image_name in untagged_image_dict:
            untagged_image = untagged_image_dict[image_name]
            score_list.append(cosine(ref_image, untagged_image))
        score_dict = dict(zip(untagged_image_dict.keys(), score_list))
        cnn_dict[query_image_name] = sorted(score_dict.items(), key=operator.itemgetter(1))

    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('Compiled cosine-similarity dict in: ' + time_display)

    return cnn_dict

def get_cnn_test_dict(vectorised_images, vectorised_audio, dtw_dict, num_untagged_images, override=False):
    """Get a dict containing a ranked list of tuples for each image file. Creates a new file if one does not exist"""
    cnn_dict = {}
    if os.path.isfile(CNN_TEST_DICT_DIR) and not override:
        cnn_dict_file = np.load(CNN_TEST_DICT_DIR)
        # convert strings to floats
        for query in cnn_dict_file:
            ranked_tuples = cnn_dict_file[query]
            cnn_dict[query] = []
            for ranked_tuple in ranked_tuples:
                new_tuple = (ranked_tuple[0], float(ranked_tuple[1])) #conversion
                cnn_dict[query].append(new_tuple)
        num_query_images = len(dtw_dict[dtw_dict.keys()[0]])
        if len(cnn_dict) != num_query_images or len(cnn_dict[cnn_dict.keys()[0]]) != num_untagged_images:
            cnn_dict = make_cnn_test_dict(vectorised_images, vectorised_audio, dtw_dict, num_untagged_images)
            np.savez_compressed(CNN_TEST_DICT_DIR, **cnn_dict)
    else:
        cnn_dict = make_cnn_test_dict(vectorised_images, vectorised_audio, dtw_dict, num_untagged_images)
        np.savez_compressed(CNN_TEST_DICT_DIR, **cnn_dict)
    
    return cnn_dict

def get_cnn_FPR_TPR(targets, labels_dict, ranked_tuples, threshold, rev_thresh=False):
    """Return the FPR & TPR for a cosine-similarity ranked list of tuples at some threshold"""
    # ranked_tuple[0] is name
    # ranked_tuple[1] is score

    if not rev_thresh:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] > threshold]
    else:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] < threshold]
    
    TP = [name for name in selected if any([target for target in targets if target in labels_dict[name]])] # True Positives
    P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if any([target for target in targets if target in labels_dict[ranked_tuple[0]]])] # all Positives

    TPR = float(len(TP))/float(len(P)) # True Positive Rate
    FPR = float((len(selected)-len(TP)))/float((len(ranked_tuples)-len(P))) # False Positive Rate

    return FPR, TPR

def get_cnn_metrics(cnn_dict, dtw_dict, ROC_res=100):
    """Calculate P@10, P@N, ROC curve and EER for image cosine-similarity comparison"""
    p_at_10 = 0.0
    p_at_N = 0.0
    FPR_axis = []
    TPR_axis = []
    EER_index = 0

    # Get dictionary of labels
    labels_dict = dict(np.load(IMAGE_LABELS_DIR)) #TODO: a test

    missing_labels = []
    for query in cnn_dict:
        if query not in labels_dict.keys():
            if query not in missing_labels:
                missing_labels.append(query)
    for ranked_tuple in cnn_dict[cnn_dict.keys()[0]]:
        name = ranked_tuple[0]
        if name not in labels_dict.keys():
            if name not in missing_labels:
                missing_labels.append(name)
    print("Detected " + str(len(missing_labels)) + " missing labels for working data")
    for label in missing_labels:
        print label
    # Placeholders for missing labels
    for image_name in missing_labels:
        labels_dict[image_name] = [image_name] #its own name wont match any labels

    # P@10
    start_time = time.time()
    p_at_10_results = []
    for query in cnn_dict:
        ranked_tuples = cnn_dict[query]
        targets = labels_dict[query]
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:10]]
        b_missing_labels = any(name for name in selected if name not in labels_dict.keys())
        if not b_missing_labels:
            TP = [name for name in selected if any([target for target in targets if target in labels_dict[name]])] # True Positives
            precision = float(len(TP))/float(len(selected)) #if len(selected) != 0 else 0 # how many of the returned results correct?
            p_at_10_results.append(precision)
    p_at_10 = np.mean(p_at_10_results) #Average over trial results
    p_at_10 = round(p_at_10, 2) # Round to 2 decimal places
    std_dev_10 = np.std(p_at_10_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@10: ' + str(p_at_10) + ' with std dev of: ' + str(std_dev_10) + ' - completed in ' + time_display)

    # P@N
    start_time = time.time()
    p_at_N_results = []
    for query in cnn_dict:
        ranked_tuples = cnn_dict[query]
        targets = labels_dict[query]
        P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if any([target for target in targets if target in labels_dict[ranked_tuple[0]]])] # all Positives
        N = len(P)
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:N]]
        b_missing_labels = any(name for name in selected if name not in labels_dict.keys())
        if not b_missing_labels:
            TP = [name for name in selected if any([target for target in targets if target in labels_dict[name]])] # True Positives
            precision = float(len(TP))/float(len(selected)) #if len(selected) != 0 else 0 # how many of the returned results correct?
            p_at_N_results.append(precision)
    p_at_N = np.sum(p_at_N_results)/float(len(p_at_N_results)) #Average over trial results
    p_at_N = round(p_at_N, 2) # Round to 2 decimal places
    std_dev_N = np.std(p_at_N_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@N: ' + str(p_at_N) + ' with std dev of: ' + str(std_dev_N) + ' - completed in ' + time_display)

    # ROC Curve
    start_time = time.time()
    TPR_axes = []
    FPR_axes = []
    # For each query, ROC
    for query in cnn_dict:
        ranked_tuples = cnn_dict[query]
        targets = labels_dict[query]
        # Form ROC by getting TPR and FPR for each threshold
        thresholds = np.arange(0,1,(1.0/ROC_res))
        TPR_axis = []
        FPR_axis = []
        for threshold in thresholds:
            FPR, TPR = get_cnn_FPR_TPR(targets, labels_dict, ranked_tuples, threshold, rev_thresh=True)
            TPR_axis.append(TPR)
            FPR_axis.append(FPR)
        TPR_axes.append(TPR_axis)
        FPR_axes.append(FPR_axis)
    # Average over the trials
    TPR_axis = np.mean(np.array(TPR_axes), axis=0)
    FPR_axis = np.mean(np.array(FPR_axes), axis=0)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('ROC computed in: ' + time_display)

    # EER
    eer_line = []
    for x in FPR_axis:
        eer_line.append(-x + 1)
    EER_index = np.argwhere(np.diff(np.sign(TPR_axis - eer_line))).flatten()
    print('EER threshold: ' + str(EER_index*(1.0/ROC_res)))

    return p_at_10, p_at_N, FPR_axis, TPR_axis, EER_index

def make_bb_test_dict(vectorised_audio, vectorised_images, image_ranking_function, num_queries, tagged_image_batch_size, untagged_image_batch_size, N=10):
    """Make a black-box testing dictionary, saves and then returns the dict"""
    start_time = time.time()
    print('Computing new black-box testing dict...')

    # Get labels dictionary
    labels_dict = np.load(IMAGE_LABELS_DIR)

    # Get audio query batch, //each containing unique image-name - NOT IMPLEMENTED
    audio_query_names = random.sample(vectorised_audio.keys(), num_queries)
    audio_queries = {}
    for name in audio_query_names:
        audio_queries[name] = vectorised_audio[name]

    # Get tagged-images batch //(audio queries containing unique, audio-tagged-image-name) - NOT IMPLEMENTED
    image_tag_names = random.sample([name for name in vectorised_audio.keys() if name not in audio_query_names], tagged_image_batch_size)
    image_tags = {}
    for name in image_tag_names:
        image_tags[name] = vectorised_audio[name]

    # Get untagged-images batch, labels must exist
    untagged_image_names = get_untagged_image_names(vectorised_audio.keys(), vectorised_images.keys())
    untagged_image_names = random.sample([name for name in untagged_image_names if name in labels_dict], untagged_image_batch_size)
    #untagged_images = {}
    #for name in untagged_image_names:
    #    untagged_images[name] = vectorised_images[name]

    # Compute DTW queries
    print('Computing DTW dict')
    dtw_dict = {}
    for name in audio_queries:
        dtw_costs = audio_query(audio_queries[name], image_tags)
        dtw_dict[name] = dtw_costs

    # For each audio query result list
    print('Computing image similarity scores')
    score_dict = {}
    for query in dtw_dict:
        # Get image names from results
        ref_image_names = ['_'.join(ranked_tuple[0].split('_')[2:4]) for ranked_tuple in dtw_dict[query] if '_'.join(ranked_tuple[0].split('_')[2:4]) in vectorised_images]
        # Rank the untagged images against results using function
        score_dict[query] = image_ranking_function(vectorised_images, ref_image_names, untagged_image_names, N)

    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('Compiled black-box dict in: ' + time_display)

    return score_dict

def get_bb_test_dict(vectorised_audio, vectorised_images, image_ranking_function, num_queries, tagged_image_batch_size, untagged_image_batch_size, override=False, N=10):
    bb_dict = {}
    if os.path.isfile(BB_TEST_DICT_DIR) and not override:
        bb_dict_file = np.load(BB_TEST_DICT_DIR)
        # convert strings to floats
        for query in bb_dict_file:
            ranked_tuples = bb_dict_file[query]
            bb_dict[query] = []
            for ranked_tuple in ranked_tuples:
                new_tuple = (ranked_tuple[0], float(ranked_tuple[1])) #conversion
                bb_dict[query].append(new_tuple)
        if len(bb_dict) != num_queries or len(bb_dict[bb_dict.keys()[0]]) != untagged_image_batch_size:
            bb_dict = make_bb_test_dict(vectorised_audio, vectorised_images, image_ranking_function, num_queries, tagged_image_batch_size, untagged_image_batch_size, N)
            np.savez_compressed(BB_TEST_DICT_DIR, **bb_dict)
    else:
        bb_dict = make_bb_test_dict(vectorised_audio, vectorised_images, image_ranking_function, num_queries, tagged_image_batch_size, untagged_image_batch_size, N)
        np.savez_compressed(BB_TEST_DICT_DIR, **bb_dict)
    
    return bb_dict

def get_bb_FPR_TPR(target, labels_dict, ranked_tuples, threshold, rev_thresh=False):
    """Return the FPR & TPR for a black-box ranked list of tuples (audio query and ranked untagged images) at some threshold"""
    # ranked_tuple[0] is image file name
    # ranked_tuple[1] is score

    if rev_thresh:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] < threshold]
    else:
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples if ranked_tuple[1] > threshold]

    TP = [name for name in selected if target in labels_dict[name]]
    P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if target in labels_dict[ranked_tuple[0]]]

    TPR = float(len(TP))/float(len(P)) if len(P) != 0 else 0.0 # True Positive Rate
    FPR = float((len(selected)-len(TP)))/float((len(ranked_tuples)-len(P))) # False Positive Rate

    return FPR, TPR
    
def get_bb_metrics(bb_dict, ROC_res=100):
    """Calculate P@10, P@N, ROC curve and EER for black-box simulations (checks if audio keyword is in untagged image labels)"""
    p_at_10 = 0.0
    p_at_N = 0.0
    FPR_axis = []
    TPR_axis = []
    EER_index = 0

    # Get dictionary of labels
    labels_dict = np.load(IMAGE_LABELS_DIR)

    # P@10
    start_time = time.time()
    p_at_10_results = []
    for query in bb_dict:
        ranked_tuples = bb_dict[query]
        target = query.split('_')[0]
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:10]]
        TP = [name for name in selected if target in labels_dict[name]] # True Positives
        precision = float(len(TP))/float(len(selected)) #if len(selected) != 0 else 0 # how many of the returned results correct?
        p_at_10_results.append(precision)
    p_at_10 = np.mean(p_at_10_results) #Average over trial results
    p_at_10 = round(p_at_10, 2) # Round to 2 decimal places
    std_dev_10 = np.std(p_at_10_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@10: ' + str(p_at_10) + ' with std dev of: ' + str(std_dev_10) + ' - completed in ' + time_display)

    # P@N
    start_time = time.time()
    p_at_N_results = []
    for query in bb_dict:
        ranked_tuples = bb_dict[query]
        target = query.split('_')[0]
        P = [ranked_tuple[0] for ranked_tuple in ranked_tuples if target in labels_dict[ranked_tuple[0]]] # all Positives
        N = len(P)
        selected = [ranked_tuple[0] for ranked_tuple in ranked_tuples[:N]] # select first N
        TP = [name for name in selected if target in labels_dict[name]] # True Positives
        precision = float(len(TP))/float(len(selected)) if len(selected) != 0 else 0 # how many of the returned results correct?
        p_at_N_results.append(precision)
    p_at_N = np.mean(p_at_N_results) #Average over trial results
    p_at_N = round(p_at_N, 2) # Round to 2 decimal places
    std_dev_N = np.std(p_at_N_results)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('P@N: ' + str(p_at_N) + ' with std dev of: ' + str(std_dev_N) + ' - completed in ' + time_display)

    # ROC Curve
    start_time = time.time()
    TPR_axes = []
    FPR_axes = []
    # For each query, ROC
    for query in bb_dict:
        ranked_tuples = bb_dict[query]
        target = query.split('_')[0]
        # Form ROC by getting TPR and FPR for each threshold
        thresholds = np.arange(0,1,(1.0/ROC_res))
        TPR_axis = []
        FPR_axis = []
        for threshold in thresholds:
            FPR, TPR = get_bb_FPR_TPR(target, labels_dict, ranked_tuples, threshold, rev_thresh=True)
            TPR_axis.append(TPR)
            FPR_axis.append(FPR)
        TPR_axes.append(TPR_axis)
        FPR_axes.append(FPR_axis)
    # Average over the trials
    TPR_axis = np.mean(np.array(TPR_axes), axis=0)
    FPR_axis = np.mean(np.array(FPR_axes), axis=0)
    elapsed_time = time.time() - start_time
    time_display = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    print('ROC computed in: ' + time_display)

    # EER
    eer_line = []
    for x in FPR_axis:
        eer_line.append(-x + 1)
    EER_index = np.argwhere(np.diff(np.sign(TPR_axis - eer_line))).flatten()
    print('EER threshold: ' + str(EER_index*(1.0/ROC_res)))

    return p_at_10, p_at_N, FPR_axis, TPR_axis, EER_index