#!/usr/bin/env python

"""
Split a given subset into isolated (content) words.

Author: Herman Kamper
Contact: kamperh@gmail.com
Date: 2017, 2018
"""

from datetime import datetime
from os import path
from nltk.corpus import stopwords
import argparse
import numpy as np
import sys

ctm_fn = "flickr_8k.ctm"
min_dur = 0.40  # minimum duration in seconds
min_char = 0    # minimum number of characters in word token


#-----------------------------------------------------------------------------#
#                              UTILITY FUNCTIONS                              #
#-----------------------------------------------------------------------------#

def ctm_to_dict(ctm_fn):
    """
    Return a dictionary with a list of (start, dur, word) for each utterance.
    """
    ctm_dict = {}
    with open(ctm_fn, "r") as f:
        for line in f:
            utt, _, start, dur, word = line.strip().split(" ")
            if not utt in ctm_dict:
                ctm_dict[utt] = []
            start = float(start)
            dur = float(dur)
            if not "<" in word:
                ctm_dict[utt].append((start, dur, word.lower()))
    return ctm_dict


#-----------------------------------------------------------------------------#
#                                MAIN FUNCTION                                #
#-----------------------------------------------------------------------------#

def main():
    print(datetime.now())

    # Transcriptions
    print("Reading:", ctm_fn)
    ctm_dict = ctm_to_dict(ctm_fn)

    # Isolated words
    print(datetime.now())
    print("Getting isolated words")
    token_dict = {}
    for caption in ctm_dict:
        image_name_segments = caption.split('_')
        image_name = ('_'.join([image_name_segments[0],image_name_segments[1]]))[:-4]
        for start, dur, word in ctm_dict[caption]:
            if (word not in stopwords.words("english") and dur >= min_dur and
                    len(word) >= min_char):
                if image_name not in token_dict:
                    token_dict[image_name] = []
                if word not in token_dict[image_name]:
                    token_dict[image_name].append(word)
    print("Dict length: ", len(token_dict))
    print(datetime.now())

    tokens_dict_fn = "image_labels.npz"
    print("Writing:", tokens_dict_fn)
    np.savez(tokens_dict_fn, **token_dict)

    print(datetime.now())

if __name__ == "__main__":
    main()
