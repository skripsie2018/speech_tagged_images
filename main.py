from speech_tagged_images.utils import setupDirs, setupVecData, printSampleFilenames, getQueryName, audio_query

#hello = tf.constant('Hello, TensorFlow!')
#sess = tf.Session()
#print(sess.run(hello))

# Get arguments                             DONE
    # Path to images (jpgs)                 DONE
    # Path to audio (format?)               DONE
# (Init) vectorise images (save as file)    DONE
# (Init) vectorise audio (save as file)     DONE (further testing req)
# query arg -> get ranked image names by comparing query to captions    CURRENT
# Display ranked audio matches & query                                  PENDING (high priority)
# compare captionless image vectors to best-match image vector(s) (optional aggregration)   PENDING (high priority)
# Display ranked image set(s)               PENDING (high priority)

#---------#
# UTILITY #
#---------#

NUM_RANDOM_ENTRIES = 3
NUM_COSTS = 15

#------#
# MAIN #
#------#

def main():

    # Get raw data directories
    images_dir, audio_dir = setupDirs()

    # Get vectorised data
    vectorised_images, vectorised_audio_mfcc = setupVecData(images_dir, audio_dir)

    # Choose query audio
    printSampleFilenames(vectorised_images, vectorised_audio_mfcc, NUM_RANDOM_ENTRIES)
    query = getQueryName(vectorised_audio_mfcc)
    dtw_costs = audio_query(query, vectorised_audio_mfcc) #TODO: choose MFCC/FBANKS
    for i in range(NUM_COSTS):
        print(dtw_costs[i])

if __name__ == "__main__":
    main()