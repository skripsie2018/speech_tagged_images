from distutils.core import setup

setup(
    name='SpeechTaggedImages',
    version='0.1dev',
    packages=['speech_tagged_images'],
)